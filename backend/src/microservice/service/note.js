import { v1 as uuidv1 } from 'uuid'
import path from 'path'
import grpc from 'grpc'
const protoLoader = require("@grpc/proto-loader")
import config from '../../config/service'
const PROTO_PATH = path.join(__dirname, '../../proto/note.proto')

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
})

// Load in our service definition
const noteProto = grpc.loadPackageDefinition(packageDefinition).note

const notes = [
    { id: '1', title: 'Note 1', content: 'Content 1'},
    { id: '2', title: 'Note 2', content: 'Content 2'}
]
const server = new grpc.Server()

// Implement the list function
const List = (call, callback) => {
    callback(null, {notes: notes})
}
// Implement the insert function
const Create = (call, callback) => {
    let note = call.request
    // data validation
    // ...
    note.id = uuidv1()
    notes.push(note)
    callback(null, note)
}
// Implement the read function
const Read = (call, callback) => {
    let id = call.request.id
    // data validation
    // ...
    let existingNoteIndex = notes.findIndex((n) => n.id == id)
    if (existingNoteIndex != -1) {
        callback(null, notes[existingNoteIndex])
    } else {
        callback({
            code: grpc.status.NOT_FOUND,
            details: "Not found"
        })
    }
}
// Implement the update function
const Update = (call, callback) => {
    let id = call.request.id
    // data validation
    // ...
    let existingNoteIndex = notes.findIndex((n) => n.id == id)
    if (existingNoteIndex != -1) {
        notes[existingNoteIndex] = call.request
        callback(null, notes[existingNoteIndex])
    } else {
        callback({
            code: grpc.status.NOT_FOUND,
            details: "Not found"
        })
    }
}
// Implement the delete function
const Delete = (call, callback) => {
    let id = call.request.id
    // data validation
    // ...
    let existingNoteIndex = notes.findIndex((n) => n.id == id)
    if (existingNoteIndex != -1) {
        notes.splice(existingNoteIndex, 1)
        callback(null, {})
    } else {
        callback({
            code: grpc.status.NOT_FOUND,
            details: "Not found"
        })
    }
}

const exposedFunctions = {
    List,
    Create,
    Read,
    Update,
    Delete
}

server.addService(noteProto.NoteService.service, exposedFunctions)
server.bind(config.note.host +':'+ config.note.port, grpc.ServerCredentials.createInsecure())
console.log('Server running at ' + config.note.host +':'+ config.note.port)
server.start()