import path from 'path'
import grpc from 'grpc'
const protoLoader = require("@grpc/proto-loader") // ez csak require-al működik
import config from '../../config/service'
import db from '../../microservice/database/connect'
import ContactModel from '../../microservice/database/model/contact'

const PROTO_PATH = path.join(__dirname, '../../proto/contact.proto')

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
})

// Load in our service definition
const contactProto = grpc.loadPackageDefinition(packageDefinition).contact
const server = new grpc.Server()

// Deklaráljuk a modelt
const contactModel = ContactModel(db)

// Implement the list function
const List = async (call, callback) => {
    const Op = db.Sequelize.Op
    const first_name = 'Ford'
    const condition = first_name ? { first_name: { [Op.like]: `%${first_name}%` } } : null;

    // Kontakt listázása adatbázisból
    try{
        //const result = await contactModel.findAll({ where: condition })
        const result = await contactModel.findAll()
        callback(null, {contacts: result})
    }
    catch(err){
        callback({
            code: grpc.status.ABORTED,
            details: "Aborted"
        })
    }
}
// Implement the insert function
const Create = async (call, callback) => {
    let contact = call.request
    // data validation
    // ...
    // Kontakt mentése adatbázisba
    try{
        let result = await contactModel.create(contact)
        callback(null, result)
    }catch(err){
        callback({
            code: grpc.status.ABORTED,
            details: "Aborted"
        })
    }
}
// Implement the read function
const Read = async (call, callback) => {
    let id = call.request.id
    // data validation
    // ...
    // Kontakt mentése adatbázisba
    try{
        let result = await contactModel.findByPk(id)
        if(result){
            callback(null, result)
        }
        else{
            callback({
                code: grpc.status.NOT_FOUND,
                details: "Not found"
            })
        }
    }catch(err){
        callback({
            code: grpc.status.ABORTED,
            details: "Aborted"
        })
    }
}
// Implement the update function
const Update = async (call, callback) => {
    let contact = call.request
    // data validation
    // ...
    // Kontakt mentése adatbázisba
    try{
        let affectedRows = await contactModel.update(
            {
                "first_name":   contact.first_name, 
                "last_name":    contact.last_name, 
                "title":        contact.title, 
                "email":        contact.email
            },
            {
                where: { id: contact.id }
            }
        )
        if(affectedRows[0]){
            callback(null, affectedRows)
        }
        else{
            callback({
                code: grpc.status.NOT_FOUND,
                details: "Not found"
            })
        }
    }catch(err){
        callback({
            code: grpc.status.ABORTED,
            details: "Aborted"
        })
    }
}
// Implement the delete function
const Delete = async (call, callback) => {
    let id = call.request.id
    // data validation
    // ...
    // Kontakt mentése adatbázisba
    try{
        let result = await contactModel.destroy({ where: { "id": id } })
        // result értéke a törölt itemek száma
        if(result){
            callback(null, result)
        }
        else{
            callback({
                code: grpc.status.NOT_FOUND,
                details: "Not found"
            })
        }
    }catch(err){
        callback({
            code: grpc.status.ABORTED,
            details: "Aborted"
        })
    }
}

const exposedFunctions = {
    List,
    Create,
    Read,
    Update,
    Delete
}

server.addService(contactProto.ContactService.service, exposedFunctions)
server.bind(config.contact.host +':'+ config.contact.port, grpc.ServerCredentials.createInsecure())

// Létrehozzuk a modelhez tartozó táblát amennyiben még nem létezik
// Termékben nem ajánlott ez a módszer
// Fjelsztésnél eldobjuk újra tesszük akkor await db.sequelize.sync({ force: true })
db.sequelize.sync().then(() => {
    console.log("Re-sync db.")
    server.start()
    console.log('Server running at ' + config.contact.host +':'+ config.contact.port)
})
.catch(err => {
    console.log('Can not start server at ' + config.contact.host +':'+ config.contact.port)
    console.log(err)
})