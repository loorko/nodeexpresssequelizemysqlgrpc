const ContactModel = ({
    sequelize, 
    Sequelize
  }) => {
    const {INTEGER, STRING, FLOAT, BOOLEAN, DATE} = Sequelize
    const Contact = sequelize.define("contact", {
      id: {
        type: INTEGER, 
        primaryKey: true, 
        autoIncrement: true
      },
      first_name: {
        type: STRING,
        allowNull: false
      },
      last_name: {
        type: STRING
      },
      title: {
        type: STRING
      },
      email: {
        type: STRING,
        allowNull: false
      },
    });  
    return Contact;
  }
  
  export default ContactModel