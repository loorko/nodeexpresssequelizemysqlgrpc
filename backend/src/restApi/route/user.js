import express from 'express'
const router = express.Router()
import user from '../controller/user'

// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})

// GET request for list of all user items
router.get('/', user.list)

export default router