import express from 'express'
const router = express.Router()
import note from '../controller/note'

// Classic CRUD solution
// Function	    Request Method
// list	        GET
// get	        GET
// create	    POST
// update	    PUT
// delete	    DELETE
// set	        PATCH

// GET request for list of all items
router.get('/', note.list)
// POST request for create an item
router.post('/', note.create)
// GET request for read an item by id
router.get('/:id', note.read)
// GET request for update an item by id
router.put('/:id', note.update)
// GET request for delete item by id
router.delete('/:id', note.delete)
export default router