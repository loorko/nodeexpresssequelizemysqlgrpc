import express from 'express'
const router = express.Router()
import calc from '../controller/calc'

// Classic CRUD solution
// Function	    Request Method
// plus	        GET

// GET request for a plus b
router.get('/', calc.calcValidationRules('plus'), calc.validate, calc.plus)
export default router