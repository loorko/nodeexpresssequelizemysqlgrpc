import express from 'express'
const router = express()
import calc from './calc'
import user from './user'
import note from './note'
import contact from './contact'

// Documentation
// https://expressjs.com/en/api.html#router

// Számológép
router.use('/calc', calc)
// Felhasználó kezelő útvonalak
router.use('/user', user)
// Jegyzet kezelő útvonalak
router.use('/note', note)
// Kapcsolat kezelő útvonalak
router.use('/contact', contact)

export default router