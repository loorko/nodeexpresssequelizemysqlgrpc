import express from 'express'
const router = express.Router()
import contact from '../controller/contact'

// Classic CRUD solution
// Function	    Request Method
// list	        GET
// get	        GET
// create	    POST
// update	    PUT
// delete	    DELETE
// set	        PATCH

// GET request for list of all items
router.get('/', contact.list)
// POST request for create an item
router.post('/', contact.create)
// GET request for read an item by id
router.get('/:id', contact.read)
// GET request for update an item by id
router.put('/:id', contact.update)
// GET request for delete item by id
router.delete('/:id', contact.delete)
export default router