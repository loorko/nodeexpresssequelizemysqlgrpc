import axios from 'axios'

exports.list = async (req, res, next) => {
    try {
        const { data } = await axios.get("https://reqres.in/api/users?page=1")
        res.status(200).json(data)
    } catch (error) {
        res.status(500).json(error)
    }
}