import path from 'path'
import grpc from 'grpc'
const protoLoader = require("@grpc/proto-loader")
import config from '../../config/service'
const PROTO_PATH = path.join(__dirname, '../../proto/note.proto')

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
})

// Load in our service definition
const noteProto = grpc.loadPackageDefinition(packageDefinition).note
const client = new noteProto.NoteService(config.note.host +':'+ config.note.port, grpc.credentials.createInsecure())

const noteList = (options) => {
    return new Promise((resolve, reject) => {
      client.List(options, (error, response) => {
            if (error) { reject(error) }
            resolve(response)
        })
    })
}

exports.list = async (req, res, next) => {
    try{
        let result = await noteList()
        res.status(200).json(result)
    } catch(e){
        res.json(e)
    }
}

const noteCreate = (options) => {
    return new Promise((resolve, reject) => {
      client.Create(options, (error, response) => {
            if (error) { reject(error) }
            resolve(response)
        })
    })
}

exports.create = async (req, res, next) => {
    // Some input validation...
    try{
        let result = await noteCreate({
            "title": req.body.title,
            "content": req.body.content
        })
        res.status(201).json(result)
    } catch(e){
        res.status(500).json(e)
    }
}

const noteRead = (options) => {
    return new Promise((resolve, reject) => {
      client.Read(options, (error, response) => {
            if (error) { reject(error) }
            resolve(response)
        })
    })
}

exports.read = async (req, res, next) => {
    // Some input validation...
    try{
        let result = await noteRead({
            "id": req.params.id
        })
        res.status(200).json(result)
    } catch(e){
        console.log(e)
        if(e.details === 'Not found'){
            res.status(204).json(e)
        }
        else{
            res.status(500).json(e)
        }
    }
}

const noteUpdate = (options) => {
    return new Promise((resolve, reject) => {
      client.Update(options, (error, response) => {
            if (error) { reject(error) }
            resolve(response)
        })
    })
}

exports.update = async (req, res, next) => {
    // Some input validation...

    try{
        let result = await noteUpdate({
            "id": req.params.id,
            "title": req.body.title,
            "content": req.body.content
        })
        res.status(200).json(result)
    } catch(e){
        if(e.details === 'Not found'){
            res.status(204).json(e)
        }
        else{
            res.status(500).json(e)
        }
    }
}

const noteDelete = (options) => {
    return new Promise((resolve, reject) => {
      client.Delete(options, (error, response) => {
            if (error) { reject(error) }
            resolve(response)
        })
    })
}

exports.delete = async (req, res, next) => {
    // Some input validation...

    try{
        let result = await noteDelete({
            "id": req.params.id
        })
        res.status(200).json(result)
    } catch(e){
        if(e.details === 'Not found'){
            res.status(204).json(e)
        }
        else{
            res.status(500).json(e)
        }
    }
}