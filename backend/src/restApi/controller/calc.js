import { body, validationResult } from 'express-validator'

exports.calcValidationRules = (method) => {
    switch (method) {
        case 'plus': {
            return [
                body('a').isNumeric(),
                body('b').isNumeric()
            ]
        }
    }
}

exports.validate = (req, res, next) => {
    const errors = validationResult(req)
    if (errors.isEmpty()) {
        return next()
    }
    const extractedErrors = []
    errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }))
  
    return res.status(400).json({
        errors: extractedErrors
    })
}

exports.plus = (req, res, next) => {
    const result = req.body.a+req.body.b
    res.status(200).json({result: result})
}