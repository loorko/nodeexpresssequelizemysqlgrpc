import path from 'path'
import grpc from 'grpc'
const protoLoader = require("@grpc/proto-loader")
import config from '../../config/service'
const PROTO_PATH = path.join(__dirname, '../../proto/contact.proto')

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
})

// Load in our service definition
const contactProto = grpc.loadPackageDefinition(packageDefinition).contact
const client = new contactProto.ContactService(config.contact.host +':'+ config.contact.port, grpc.credentials.createInsecure())

const contactList = (options) => {
    return new Promise((resolve, reject) => {
      client.List(options, (error, response) => {
            if (error) { reject(error) }
            resolve(response)
        })
    })
}

exports.list = async (req, res, next) => {
    try{
        let result = await contactList()
        res.status(200).json(result)
    } catch(e){
        res.json(e)
    }
}

const contactCreate = (options) => {
    return new Promise((resolve, reject) => {
      client.Create(options, (error, response) => {
            if (error) { reject(error) }
            resolve(response)
        })
    })
}

exports.create = async (req, res, next) => {
    // Some input validation...
    try{
        let result = await contactCreate({
            "first_name": req.body.first_name, 
            "last_name": req.body.last_name, 
            "title": req.body.title, 
            "email": req.body.email
        })
        res.status(201).json(result)
    } catch(e){
        res.status(500).json(e)
    }
}

const contactRead = (options) => {
    return new Promise((resolve, reject) => {
      client.Read(options, (error, response) => {
            if (error) { reject(error) }
            resolve(response)
        })
    })
}

exports.read = async (req, res, next) => {
    // Some input validation...
    try{
        let result = await contactRead({
            "id": req.params.id
        })
        res.status(200).json(result)
    } catch(e){
        if(e.details === 'Not found'){
            res.status(204).json(e)
        }
        else{
            res.status(500).json(e)
        }
    }
}

const contactUpdate = (options) => {
    return new Promise((resolve, reject) => {
      client.Update(options, (error, response) => {
            if (error) { reject(error) }
            resolve(response)
        })
    })
}

exports.update = async (req, res, next) => {
    // Some input validation...

    try{
        let result = await contactUpdate({
            "id": req.params.id,
            "first_name": req.body.first_name, 
            "last_name": req.body.last_name, 
            "title": req.body.title, 
            "email": req.body.email
        })
        res.status(200).json()
    } catch(e){
        if(e.details === 'Not found'){
            res.status(204).json(e)
        }
        else{
            res.status(500).json(e)
        }
    }
}

const contactDelete = (options) => {
    return new Promise((resolve, reject) => {
      client.Delete(options, (error, response) => {
            if (error) { reject(error) }
            resolve(response)
        })
    })
}

exports.delete = async (req, res, next) => {
    // Some input validation...

    try{
        let result = await contactDelete({
            "id": req.params.id
        })
        res.status(200).json()
    } catch(e){
        if(e.details === 'Not found'){
            res.status(204).json(e)
        }
        else{
            res.status(500).json(e)
        }
    }
}