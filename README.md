# Javascript alapú arhitektúra demo
---
Jelen repó bemutatja:

* Rest backend megvalósítását Nodejs + Express alapon
* Microservice backend megvalósítását gRPC kommunikációval
* Mysql adatbázis műveleteket ORM-el

Pirossal keretezett rész fejlesztés alatt, mely bemutatja majd:

* RabitMQ kummunkációt


## Rendszer felépítése
---
![alt text](https://stash.automizy.com/projects/SAN/repos/expressrestapigrpcmicroservice/raw/doc/image/arhitect.png "Arhitect")


## Telepítési lépések
---
### Repó klónozása

```
git clone ssh://git@stash.automizy.com:7999/san/expressrestapigrpcmicroservice.git
```


### Backend installálása
#### Rest + Microservice réteghez
```
cd backend
```
```
yarn install
```


#### Mysql docker konténer futattsához
```
cd database
```
```
curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```
```
chmod +x /usr/local/bin/docker-compose
```


## Rest backend megvalósítása Nodejs + Express alapon
---
Amennyiben csak a Rest réteget szertnénk kipróbálni akkor két fő végpont érhető el a jelen demó alkalmazásban:

* /api/calc : bemutatja azt az esetet mikor a rest kontroller rétegen csak valami üzleti logikát kell intézni
* /api/user : bemutatja azt az esetet mikor a rest kontroller rétegen át egy harmadik fél api-ját kell meghívni

### Első példa

Bemutatja azt az esetet mikor a rest kontroller rétegen csak valami üzleti logikát kell intézni.

#### Rest réteg futtatás [Terminal 1]
```
cd backend
```
Indítsuk el a Rest réteget (ha még nem fut)
```
yarn start:rest
```

#### Használat [Terminal 2]

Calc
```
curl --request GET \
--url localhost:3000/api/calc \
--header 'content-type: application/json' \
--data '{
  "a": 1,
  "b": 2
}'
```
Calc error
```
curl --request GET \
--url localhost:3000/api/calc \
--header 'content-type: application/json' \
--data '{
  "a": 1
}'
```

Vagy "Postman"-el:  
Importáld be: backend/postman/ExpressMicro.postman_collection.json  
Majd:  
  Collection: ExpressMicro  
  Folder: calc  

### Második példa

Bemutatja azt az esetet mikor a rest kontroller rétegen át egy harmadik fél api-ját kell meghívni.

#### Rest réteg futtatás [Terminal 1]
```
cd backend
```
Indítsuk el a Rest réteget (ha még nem fut)
```
yarn start:rest
```

#### Használat [Terminal 2]

List user
```
curl --request GET \
--url localhost:3000/api/user \
--header 'content-type: application/json'
```

Vagy "Postman"-el:  
Importáld be: backend/postman/ExpressMicro.postman_collection.json  
Majd:  
  Collection: ExpressMicro  
  Folder: user  


## Microservice backend megvalósítása gRPC kommunikációval
---
Amennyiben a Microservice réteget is szeretnénk használni akkor egy fő Rest végpont érhető el a jelen demó alkalmazásban:

* /api/note : bemutatja azt az esetet mikor a rest kontroller rétegen keresztül egy model réteget szeretnénk elérni

#### Start Note Backend Microservice Layer [Terminal 1]
```
cd backend
```
Indítsuk el a Note Microservice réteget (ha még nem fut)
```
yarn start:micro:note
```

#### Rest réteg futtatás [Terminal 2]
```
cd backend
```
Indítsuk el a Rest réteget (ha még nem fut)
```
yarn start:rest
```

#### Használat [Terminal 3]

List note
```
curl --request GET \
--url localhost:3000/api/note \
--header 'content-type: application/json'
```
Create note
```
curl --request POST \
--url localhost:3000/api/note \
--header 'content-type: application/json' \
--data '{
  "title": "just with Express",
  "content": "body-parser"
}'
```
Read note
```
curl --request GET \
--url localhost:3000/api/note/1 \
--header 'content-type: application/json'
```
Update note
```
curl --request POST \
--url localhost:3000/api/note/2 \
--header 'content-type: application/json' \
--data '{
  "title": "Updated",
  "content": "Updated by API"
}'
```
Delete note
```
curl --request DELETE \
--url localhost:3000/api/note/1 \
--header 'content-type: application/json'
```

Vagy "Postman"-el:  
Importáld be: backend/postman/ExpressMicro.postman_collection.json  
Majd:  
  Collection: ExpressMicro  
  Folder: note  


## Microservice backend megvalósítása gRPC kommunikációval Mysql ORM-el
---
Amennyiben a Microservice réteget ORM-el szeretnénk használni akkor egy fő Rest végpont érhető el a jelen demó alkalmazásban:

* /api/contact : bemutatja azt az esetet mikor a rest kontroller rétegen keresztül egy model réteget szeretnénk elérni ami egy mysql adatbázist használ

#### Start Contact Backend Microservice Layer [Terminal 1]
```
cd backend
```
Indítsuk el a Contact Microservice réteget (ha még nem fut)
```
yarn start:micro:contact
```

#### Rest réteg futtatás [Terminal 2]
```
cd backend
```
Indítsuk el a Rest réteget (ha még nem fut)
```
yarn start:rest
```

#### Használat [Terminal 3]
```
cd databse
```
Indítsuk el az adatbázist artalmazó docker konténert
```
docker-compose up
```

#### Használat [Terminal 4]

List contact
```
curl --request GET \
--url localhost:3000/api/contact \
--header 'content-type: application/json'
```
Create contact
```
curl --request POST \
--url localhost:3000/api/contact \
--header 'content-type: application/json' \
--data '{
	"first_name": "Ford",
    "last_name": "Fearline",
    "title": "president",
    "email": "ford.fairlane@mail.com"
}'
```
Read contact
```
curl --request GET \
--url localhost:3000/api/contact/1 \
--header 'content-type: application/json'
```
Update contact
```
curl --request POST \
--url localhost:3000/api/contact/1 \
--header 'content-type: application/json' \
--data '{
	"first_name": "Zuzu",
    "last_name": "Peta7",
    "title": "singer",
    "email": "zuzu.petaz@mail.com"
}'
```
Delete contact
```
curl --request DELETE \
--url localhost:3000/api/contact/1 \
--header 'content-type: application/json'
```

Vagy "Postman"-el:  
Importáld be: backend/postman/ExpressMicro.postman_collection.json  
Majd:  
  Collection: ExpressMicro  
  Folder: contact  


# Fejlesztői jegyzet
---

## Általános funciók és hozzájuk tartozó kérés metódusok
| Function | Request Method |
|----------|----------------|
| list     | GET            |
| get      | GET            |
| create   | POST           |
| update   | PUT            |
| set      | PATCH          |

## Állapot kódok

| Status Code | Meaning                 | Example for use     |
|-------------|-------------------------|---------------------|
| 200         | OK                      |   |
| 201         | Created                 |   |
| 204         | No content              |   |
| 400         | Bad request             | Validation error    |
| 401         | Unauthorized            | Authentication error|
| 403         | Forbidden               |   |
| 404         | Not found               |   |
| 405         | Method Not Allowed      |   |
| 409         | Conflict                | Duplication error   |
| 500         | Internal Server Error   |   |