import cron from 'node-cron'
import express from 'express'

const app = express()
const port = process.env.PORT || 3000

// schedule tasks to be run on the server
cron.schedule("*/2 * * * * *", function() {
    console.log("running a task every 2th sec");
});

app.listen(port, function() {
    console.log('Server started on port: ' + port);
})