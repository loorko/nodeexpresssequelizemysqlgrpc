import React from 'react'
import { Layout, Menu } from 'antd'
import Rest from '../page/Rest'
const { Header, Content } = Layout


class App extends React.Component {
  state = {
    current: 'rest'
  };

  handleClick = e => {
    this.setState({
      current: e.key
    })
  }

  render() {
    return (
    <Layout className="layout">
        <Header>
            <Menu 
                onClick={this.handleClick} 
                selectedKeys={[this.state.current]} 
                theme="dark"
                mode="horizontal">
                <Menu.Item key="rest">
                    Controller
                </Menu.Item>
                <Menu.Item key="micro">
                    GRPC Microserice
                </Menu.Item>
            </Menu>
        </Header>
        <Content style={{ padding: '0 50px' }}>
            <Rest/>
        </Content>
    </Layout>
    )
  }
}

export default App