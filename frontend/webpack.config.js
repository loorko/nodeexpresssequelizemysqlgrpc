const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");

const htmlPlugin = new HtmlWebPackPlugin({
    template: "./src/index.html",
    filename: "./index.html"
});
module.exports = {
    mode: 'development',
    entry: './src/app.js',
    output: {
        filename: '[name].[contenthash].js',
        path: path.resolve(__dirname, 'public'),
        publicPath: '/'
    },
    module: {
        rules: [{   
            test: /\.(js|jsx)$/,
            use: { 
                loader: 'babel-loader',
                options: {
                    babelrc: false,
                    presets: [
                        "@babel/env",
                        "@babel/react"
                    ],
                    plugins: [
                        "@babel/plugin-transform-runtime",
                        "@babel/proposal-class-properties",
                        "@babel/plugin-proposal-export-default-from",
                        "babel-plugin-add-react-displayname",
                        ["import", { "libraryName": "antd", "libraryDirectory": "es", "style": "css" }] // `style: true` for less
                    ]
                }
            },
            exclude: /node_modules/
        },
        {
            test: /\.s?css$/,
            use: [
              'style-loader',
              'css-loader',
              'sass-loader'
            ]
        }
    ]
    },
    devtool: 'cheap-module-eval-source-map',
    devServer: {
        watchOptions: {
            poll: true
        },
        contentBase: path.resolve(__dirname, 'public'),
        historyApiFallback: true
    },
    performance: {
        hints: false
    },
    plugins: [
        htmlPlugin
    ]
};